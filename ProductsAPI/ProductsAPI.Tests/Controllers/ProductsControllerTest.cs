﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductsAPI.Controllers;
using ProductsAPI.Models;
using ProductsAPI.Services;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProductsAPI.Tests.Controllers
{
    public class ProductServiceMock : IProductService
    {
       
        public List<Product> GetList()
        {
            Product product = new Product()
            {
                CodProduct = "0001",
                DesProduct = "Product 01",
                StaStatus = "1"
            };

            List<Product> products = new List<Product>();
            products.Add(product);

            return products;
        }

        public Product GetByKey(string codProduct)
        {
            Product product = new Product()
            {
                CodProduct = "0001",
                DesProduct = "Product 01",
                StaStatus = "1"
            };

            return product;
        }
    }

    public class ProductCosifServiceMock : IProductCosifService
    {
        public List<ProductCosif> GetList(string codProduct)
        {
            ProductCosif productCosif = new ProductCosif()
            {
                CodProduct = "0001",
                CodCosif = "00000000001",
                CodClassification = "C-0001",
                StaStatus = "1"
            };

            List<ProductCosif> productCosifs = new List<ProductCosif>();
            productCosifs.Add(productCosif);

            return productCosifs;
        }
    }

    [TestClass]
    public class ProductsControllerTest
    {
        [TestMethod]
        public void GetProducts()
        {
            // Arrange
            ProductsController controller = new ProductsController(new ProductServiceMock(), new ProductCosifServiceMock());

            // Act
            IHttpActionResult actionResult = controller.GetProducts();
            var contentResult = actionResult as OkNegotiatedContentResult<List<Product>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.Count);
            Assert.AreEqual("0001", contentResult.Content[0].CodProduct);
        }

        [TestMethod]
        public void GetProduct()
        {
            // Arrange
            ProductsController controller = new ProductsController(new ProductServiceMock(), new ProductCosifServiceMock());

            // Act
            IHttpActionResult actionResult = controller.GetProduct("0001");
            var contentResult = actionResult as OkNegotiatedContentResult<Product>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual("0001", contentResult.Content.CodProduct);
        }

        [TestMethod]
        public void GetProductCosifs()
        {
            // Arrange
            ProductsController controller = new ProductsController(new ProductServiceMock(), new ProductCosifServiceMock());

            // Act
            IHttpActionResult actionResult = controller.GetProductCosifs("0001");
            var contentResult = actionResult as OkNegotiatedContentResult<List<ProductCosif>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.Count);
            Assert.AreEqual("0001", contentResult.Content[0].CodProduct);
            Assert.AreEqual("00000000001", contentResult.Content[0].CodCosif);
        }
    }
}
