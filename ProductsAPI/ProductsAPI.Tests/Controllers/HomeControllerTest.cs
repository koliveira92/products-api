﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductsAPI;
using ProductsAPI.Controllers;
using System.Web.Mvc;

namespace ProductsAPI.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Organizar
            HomeController controller = new HomeController();

            // Agir
            ViewResult result = controller.Index() as ViewResult;

            // Declarar
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
