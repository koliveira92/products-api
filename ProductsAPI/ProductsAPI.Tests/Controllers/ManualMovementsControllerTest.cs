﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductsAPI.Controllers;
using ProductsAPI.Dto;
using ProductsAPI.Models;
using ProductsAPI.Services;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace ProductsAPI.Tests.Controllers
{
    public class ManualMovementServiceMock : IManualMovementService
    {
        public List<ManualMovement> GetList()
        {
            ManualMovement manualMovement = new ManualMovement()
            {
                DatMonth = 1,
                DatYear = 2020,
                NumRelease = 1,
                CodProduct = "0001",
                CodCosif = "00000000001",
                DesDescription = "Descricao 01",
                DatMovement = DateTime.Now,
                CodUser = "TESTE",
                ValValue = 10
            };

            List<ManualMovement> manualMovements = new List<ManualMovement>();
            manualMovements.Add(manualMovement);

            return manualMovements;
        }

        public ManualMovement GetByKey(int datMonth, int datYear, int numRelease, string codProduct, string codCosif)
        {
            ManualMovement manualMovement = new ManualMovement()
            {
                DatMonth = 1,
                DatYear = 2020,
                NumRelease = 1,
                CodProduct = "0001",
                CodCosif = "00000000001",
                DesDescription = "Descricao 01",
                DatMovement = DateTime.Now,
                CodUser = "TESTE",
                ValValue = 10
            };

            return manualMovement;
        }

        public ManualMovement Create(ManualMovementCreateDto manualMovementToCreate)
        {
            ManualMovement manualMovement = new ManualMovement()
            {
                DatMonth = manualMovementToCreate.DatMonth,
                DatYear = manualMovementToCreate.DatYear,
                NumRelease = 1,
                CodProduct = manualMovementToCreate.CodProduct,
                CodCosif = manualMovementToCreate.CodCosif,
                DesDescription = manualMovementToCreate.DesDescription,
                DatMovement = DateTime.Now,
                CodUser = "TESTE",
                ValValue = manualMovementToCreate.ValValue
            };

            return manualMovement;
        }
    }

    [TestClass]
    public class ManualMovementsControllerTest
    {
        [TestMethod]
        public void GetManualMovements()
        {
            // Arrange
            ManualMovementsController controller = new ManualMovementsController(new ManualMovementServiceMock());

            // Act
            IHttpActionResult actionResult = controller.GetManualMovements();
            var contentResult = actionResult as OkNegotiatedContentResult<List<ManualMovement>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.Count);
            Assert.AreEqual(1, contentResult.Content[0].DatMonth);
            Assert.AreEqual(2020, contentResult.Content[0].DatYear);
            Assert.AreEqual("0001", contentResult.Content[0].CodProduct);
            Assert.AreEqual("00000000001", contentResult.Content[0].CodCosif);
        }

        [TestMethod]
        public void PostManualMovement()
        {
            // Arrange
            ManualMovementsController controller = new ManualMovementsController(new ManualMovementServiceMock());
            ManualMovementCreateDto manualMovementToCreate = new ManualMovementCreateDto()
            {
                DatMonth = 2,
                DatYear = 2021,
                CodProduct = "0002",
                CodCosif = "00000000002",
                DesDescription = "Descricao 01",
                ValValue = 20
            };

            // Act
            IHttpActionResult actionResult = controller.PostManualMovement(manualMovementToCreate);
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<ManualMovement>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual(2, createdResult.RouteValues["id"]);
            Assert.IsNotNull(createdResult.Content);
            Assert.AreEqual(2, createdResult.Content.DatMonth);
            Assert.AreEqual(2021, createdResult.Content.DatYear);
            Assert.AreEqual("0002", createdResult.Content.CodProduct);
            Assert.AreEqual("00000000002", createdResult.Content.CodCosif);
        }
    }
}
