﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductsAPI.Models
{
    public class ProductCosif
    {
        [Key]
        [Column(Order = 1)]
        [MaxLength(4)]
        [ForeignKey("Product")]
        public string CodProduct { get; set; }

        [Key]
        [Column(Order = 2)]
        [MaxLength(11)]
        public string CodCosif { get; set; }

        [MaxLength(6)]
        public string CodClassification { get; set; }

        [MaxLength(1)]
        public string StaStatus { get; set; }

        public Product Product { get; set; }
    }
}