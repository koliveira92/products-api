﻿using System.ComponentModel.DataAnnotations;

namespace ProductsAPI.Models
{
    public class Product
    {
        [Key]
        [MaxLength(4)]
        public string CodProduct { get; set; }

        [MaxLength(30)]
        public string DesProduct { get; set; }

        [MaxLength(1)]
        public string StaStatus { get; set; }
    }
}