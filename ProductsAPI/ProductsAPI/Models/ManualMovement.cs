﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductsAPI.Models
{
    public class ManualMovement
    {
        [Key]
        [Column(Order = 1)]
        public int DatMonth { get; set; }

        [Key]
        [Column(Order = 2)]
        public int DatYear { get; set; }

        [Key]
        [Column(Order = 3)]
        public int NumRelease { get; set; }

        [Key]
        [Column(Order = 4)]
        [MaxLength(4)]
        [ForeignKey("ProductCosif")]
        public string CodProduct { get; set; }

        [Key]
        [Column(Order = 5)]
        [MaxLength(11)]
        [ForeignKey("ProductCosif")]
        public string CodCosif { get; set; }

        [Required]
        [MaxLength(50)]
        public string DesDescription { get; set; }

        [Required]
        public DateTime DatMovement { get; set; }

        [Required]
        [MaxLength(15)]
        public string CodUser { get; set; }

        [Required]
        public decimal ValValue { get; set; }

        public ProductCosif ProductCosif { get; set; }
    }
}