﻿using ProductsAPI.Models;
using System.Data.Entity;

namespace ProductsAPI.Data
{
    public class ProductsAPIContext : DbContext
    {    
        public ProductsAPIContext() : base("name=ProductsAPIContext")
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductCosif> ProductCosifs { get; set; }

        public DbSet<ManualMovement> ManualMovements { get; set; }
    }
}
