﻿using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public interface IProductCosifService
    {
        List<ProductCosif> GetList(string codProduct);
    }
}
