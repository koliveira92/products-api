﻿using ProductsAPI.Dto;
using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public class ManualMovementService : IManualMovementService
    {
        private IManualMovementRepository _manualMovementRepository;

        public ManualMovementService()
        {
            this._manualMovementRepository = new ManualMovementRepository();
        }

        public ManualMovementService(IManualMovementRepository manualMovementRepository)
        {
            this._manualMovementRepository = manualMovementRepository;
        }

        public List<ManualMovement> GetList()
        {
            return _manualMovementRepository.GetList();
        }

        public ManualMovement GetByKey(int datMonth, int datYear, int numRelease, string codProduct, string codCosif)
        {
            return _manualMovementRepository.GetByKey(datMonth, datYear, numRelease, codProduct, codCosif);
        }

        public ManualMovement Create(ManualMovementCreateDto manualMovementToCreate)
        {
            int numRelease = GetNumRelease(manualMovementToCreate.DatMonth, manualMovementToCreate.DatYear);

            ManualMovement manualMovement = new ManualMovement()
            {
                DatMonth = manualMovementToCreate.DatMonth,
                DatYear = manualMovementToCreate.DatYear,
                NumRelease = numRelease,
                CodProduct = manualMovementToCreate.CodProduct,
                CodCosif = manualMovementToCreate.CodCosif,
                DesDescription = manualMovementToCreate.DesDescription,
                DatMovement = DateTime.Now,
                CodUser = "TESTE",
                ValValue = manualMovementToCreate.ValValue
            };

            return _manualMovementRepository.Create(manualMovement);
        }

        private int GetNumRelease(int datMonth, int datYear)
        {
            ManualMovement lastManualMovementByMonthAndYear = _manualMovementRepository.GetLastByMonthAndYear(datMonth, datYear);

            if (lastManualMovementByMonthAndYear == null)
                return 1;

            return lastManualMovementByMonthAndYear.NumRelease + 1;
        }
    }
}