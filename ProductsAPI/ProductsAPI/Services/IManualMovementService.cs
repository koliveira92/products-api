﻿using ProductsAPI.Dto;
using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public interface IManualMovementService
    {
        List<ManualMovement> GetList();

        ManualMovement GetByKey(int datMonth, int datYear, int numRelease, string codProduct, string codCosif);

        ManualMovement Create(ManualMovementCreateDto manualMovementToCreate);
    }
}
