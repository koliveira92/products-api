﻿using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public interface IProductService
    {
        List<Product> GetList();

        Product GetByKey(string codProduct);
    }
}
