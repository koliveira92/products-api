﻿using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public class ProductCosifService : IProductCosifService
    {
        private IProductCosifRepository _productCosifRepository;

        public ProductCosifService()
        {
            this._productCosifRepository = new ProductCosifRepository();
        }

        public ProductCosifService(IProductCosifRepository productCosifRepository)
        {
            this._productCosifRepository = productCosifRepository;
        }

        public List<ProductCosif> GetList(string codProduct)
        {
            return _productCosifRepository.GetList(codProduct);
        }
    }
}