﻿using ProductsAPI.Models;
using ProductsAPI.Repositories;
using System.Collections.Generic;

namespace ProductsAPI.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;

        public ProductService()
        {
            this._productRepository = new ProductRepository();
        }

        public ProductService(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public List<Product> GetList()
        {
            return _productRepository.GetList();
        }

        public Product GetByKey(string codProduct)
        {
            return _productRepository.GetByKey(codProduct);
        }
    }
}