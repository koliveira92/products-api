﻿namespace ProductsAPI.Dto
{
    public class ManualMovementCreateDto
    {
        public int DatMonth { get; set; }
        public int DatYear { get; set; }
        public string CodProduct { get; set; }
        public string CodCosif { get; set; }
        public string DesDescription { get; set; }
        public decimal ValValue { get; set; }
    }
}