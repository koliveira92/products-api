﻿using ProductsAPI.Dto;
using ProductsAPI.Models;
using ProductsAPI.Services;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace ProductsAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ManualMovementsController : ApiController
    {
        private IManualMovementService _manualMovementService;

        public ManualMovementsController()
        {
            this._manualMovementService = new ManualMovementService();
        }

        public ManualMovementsController(IManualMovementService manualMovementService)
        {
            this._manualMovementService = manualMovementService;
        }

        // GET: api/ManualMovements
        [ResponseType(typeof(List<ManualMovement>))]
        public IHttpActionResult GetManualMovements()
        {
            List<ManualMovement> manualMovements = _manualMovementService.GetList();
            return Ok(manualMovements);
        }

        // POST: api/ManualMovements
        [ResponseType(typeof(ManualMovement))]
        public IHttpActionResult PostManualMovement(ManualMovementCreateDto manualMovementToCreate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ManualMovement manualMovement = _manualMovementService.Create(manualMovementToCreate);
            return CreatedAtRoute("DefaultApi", new { id = manualMovement.DatMonth }, manualMovement);
        }
    }
}