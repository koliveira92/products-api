﻿using ProductsAPI.Models;
using ProductsAPI.Services;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace ProductsAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        private IProductService _productService;
        private IProductCosifService _productCosifService;

        public ProductsController()
        {
            this._productService = new ProductService();
            this._productCosifService = new ProductCosifService();
        }

        public ProductsController(IProductService productService, IProductCosifService productCosifService)
        {
            this._productService = productService;
            this._productCosifService = productCosifService;
        }

        // GET: api/Products
        [ResponseType(typeof(List<Product>))]
        public IHttpActionResult GetProducts()
        {
            List<Product> products = _productService.GetList();
            return Ok(products);
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(string id)
        {
            Product product = _productService.GetByKey(id);
            return Ok(product);
        }

        // GET: api/Products/5/Cosifs
        [Route("api/Products/{id}/Cosifs")]
        [ResponseType(typeof(List<ProductCosif>))]
        public IHttpActionResult GetProductCosifs(string id)
        {
            List<ProductCosif> productCosifs = _productCosifService.GetList(id);
            return Ok(productCosifs);
        }
    }
}