﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public interface IProductCosifRepository
    {
        List<ProductCosif> GetList(string codProduct);
    }
}
