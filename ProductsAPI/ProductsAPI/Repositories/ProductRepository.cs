﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private ProductsAPIContext _db;

        public ProductRepository()
        {
            this._db = new ProductsAPIContext();
        }

        public ProductRepository(ProductsAPIContext db)
        {
            this._db = db;
        }

        public List<Product> GetList()
        {
            return _db.Products.ToList();
        }

        public Product GetByKey(string codProduct)
        {
            return _db.Products.Find(codProduct);
        }
    }
}