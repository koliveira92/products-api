﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public interface IManualMovementRepository
    {
        List<ManualMovement> GetList();

        ManualMovement GetByKey(int datMonth, int datYear, int numRelease, string codProduct, string codCosif);

        ManualMovement GetLastByMonthAndYear(int datMonth, int datYear);

        ManualMovement Create(ManualMovement manualMovementToCreate);
    }
}
