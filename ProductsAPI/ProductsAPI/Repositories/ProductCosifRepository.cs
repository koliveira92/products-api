﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public class ProductCosifRepository : IProductCosifRepository
    {
        private ProductsAPIContext _db;

        public ProductCosifRepository()
        {
            this._db = new ProductsAPIContext();
        }

        public ProductCosifRepository(ProductsAPIContext db)
        {
            this._db = db;
        }

        public List<ProductCosif> GetList(string codProduct)
        {
            return _db.ProductCosifs
                .Include("Product")
                .Where(x => x.CodProduct == codProduct)
                .ToList();
        }
    }
}