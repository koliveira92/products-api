﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public interface IProductRepository
    {
        List<Product> GetList();

        Product GetByKey(string codProduct);
    }
}
