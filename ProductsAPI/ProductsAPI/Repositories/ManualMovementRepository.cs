﻿using ProductsAPI.Data;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProductsAPI.Repositories
{
    public class ManualMovementRepository : IManualMovementRepository
    {
        private ProductsAPIContext _db;

        public ManualMovementRepository()
        {
            this._db = new ProductsAPIContext();
        }

        public ManualMovementRepository(ProductsAPIContext db)
        {
            this._db = db;
        }

        public List<ManualMovement> GetList()
        {
            return _db.ManualMovements
                .Include("ProductCosif")
                .Include("ProductCosif.Product")
                .OrderBy(x => x.DatMonth)
                .ThenBy(x => x.DatYear)
                .ThenBy(x => x.NumRelease)
                .ToList();
        }

        public ManualMovement GetByKey(int datMonth, int datYear, int numRelease, string codProduct, string codCosif)
        {
            return _db.ManualMovements.Find(datMonth, datYear, numRelease, codProduct, codCosif);
        }

        public ManualMovement GetLastByMonthAndYear(int datMonth, int datYear)
        {
            return _db.ManualMovements
                .Where(x => x.DatMonth == datMonth && x.DatYear == datYear)
                .OrderByDescending(x => x.DatMovement)
                .FirstOrDefault();
        }

        public ManualMovement Create(ManualMovement manualMovementToCreate)
        {
            _db.ManualMovements.Add(manualMovementToCreate);
            _db.SaveChanges();

            return manualMovementToCreate;
        }
    }
}