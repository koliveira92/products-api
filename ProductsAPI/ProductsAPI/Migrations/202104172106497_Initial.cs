namespace ProductsAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ManualMovements",
                c => new
                    {
                        DatMonth = c.Int(nullable: false),
                        DatYear = c.Int(nullable: false),
                        NumRelease = c.Int(nullable: false),
                        CodProduct = c.String(nullable: false, maxLength: 4),
                        CodCosif = c.String(nullable: false, maxLength: 11),
                        DesDescription = c.String(nullable: false, maxLength: 50),
                        DatMovement = c.DateTime(nullable: false),
                        CodUser = c.String(nullable: false, maxLength: 15),
                        ValValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.DatMonth, t.DatYear, t.NumRelease, t.CodProduct, t.CodCosif })
                .ForeignKey("dbo.ProductCosifs", t => new { t.CodProduct, t.CodCosif }, cascadeDelete: true)
                .Index(t => new { t.CodProduct, t.CodCosif });
            
            CreateTable(
                "dbo.ProductCosifs",
                c => new
                    {
                        CodProduct = c.String(nullable: false, maxLength: 4),
                        CodCosif = c.String(nullable: false, maxLength: 11),
                        CodClassification = c.String(maxLength: 6),
                        StaStatus = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => new { t.CodProduct, t.CodCosif })
                .ForeignKey("dbo.Products", t => t.CodProduct, cascadeDelete: true)
                .Index(t => t.CodProduct);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        CodProduct = c.String(nullable: false, maxLength: 4),
                        DesProduct = c.String(maxLength: 30),
                        StaStatus = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => t.CodProduct);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ManualMovements", new[] { "CodProduct", "CodCosif" }, "dbo.ProductCosifs");
            DropForeignKey("dbo.ProductCosifs", "CodProduct", "dbo.Products");
            DropIndex("dbo.ProductCosifs", new[] { "CodProduct" });
            DropIndex("dbo.ManualMovements", new[] { "CodProduct", "CodCosif" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductCosifs");
            DropTable("dbo.ManualMovements");
        }
    }
}
