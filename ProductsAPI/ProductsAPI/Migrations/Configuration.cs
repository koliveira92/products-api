namespace ProductsAPI.Migrations
{
    using ProductsAPI.Data;
    using ProductsAPI.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsAPIContext context)
        {
            context.Products.AddOrUpdate(x => x.CodProduct,
                new Product() { CodProduct = "0001", DesProduct = "Product 01", StaStatus = "1" },
                new Product() { CodProduct = "0002", DesProduct = "Product 02", StaStatus = "1" },
                new Product() { CodProduct = "0003", DesProduct = "Product 03", StaStatus = "1" }
            );

            context.ProductCosifs.AddOrUpdate(x => new { x.CodProduct, x.CodCosif },
                new ProductCosif() { CodProduct = "0001", CodCosif = "00000000001", CodClassification = "C-0001", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0001", CodCosif = "00000000002", CodClassification = "C-0001", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0001", CodCosif = "00000000003", CodClassification = "C-0001", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0002", CodCosif = "00000000004", CodClassification = "C-0002", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0002", CodCosif = "00000000005", CodClassification = "C-0002", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0002", CodCosif = "00000000006", CodClassification = "C-0002", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0003", CodCosif = "00000000007", CodClassification = "C-0003", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0003", CodCosif = "00000000008", CodClassification = "C-0003", StaStatus = "1" },
                new ProductCosif() { CodProduct = "0003", CodCosif = "00000000009", CodClassification = "C-0003", StaStatus = "1" }
            );
        }
    }
}
